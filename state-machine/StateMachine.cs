using System;
using System.Collections.Generic;
using System.Text;
using TWC.Util.Logging;

namespace EchoStarInserter
{
	/// <summary>
	/// This class represents a generic state machine that has support for transition functions.
	/// You use this object by providing state transitions to the machine, and then having it process events,
	/// which cause the transitions to occur.  Recall that there are functions that execute upon entering and exiting
	/// a state.  This class coordinates event execution and also executes transition functions as appropriate.
	/// </summary>
	public class StateMachine
	{

		#region Transition Table Members
		
		/// <summary>
		/// Information about transitions.  A transistion has a transition function, and a new state.
		/// </summary>
		private struct TransitionElement
		{
			public EventHandler TransitionFunction;
			public State NewState;

			public TransitionElement(State state, EventHandler func)
			{
				TransitionFunction = func;
				NewState = state;
			}
		}

		/// <summary>
		/// We map a named transition to a transition element.  Basically, our transitions are names like STATE1_EVENT.  So,
		/// when you add a transition, we construct a unique name that can be built easily given a state and an event.
		/// </summary>
		private Dictionary<string, TransitionElement> transitionTable = new Dictionary<string, TransitionElement>();
		
		#endregion

		#region Private Members

		/// <summary>
		/// Gee, its the current state... doofus.
		/// </summary>
		private State currentState;

		#endregion

		#region Events

		/// <summary>
		/// This is a GLOBAL event that is executed before we enter any state.
		/// </summary>
		public event Action<State> PreEnter;
		/// <summary>
		/// This is a global event that is executed after we enter a state.
		/// </summary>
		public event Action<State> PostEnter;
		/// <summary>
		/// This is a global event that is executed before we exit a state
		/// </summary>
		public event Action<State> PreExit;
		/// <summary>
		/// This is a global event that is executed after we exit a state (but before entering another)
		/// </summary>
		public event Action<State> PostExit;
		/// <summary>
		/// This is a global event that is executed just before a transition function
		/// </summary>
		public event Action<State> PreTransition;
		/// <summary>
		/// This is a global event that is executed right after a transition function has executed.
		/// </summary>
		public event Action<State> PostTransition;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the current state, which will of course execute enter/exit functions.
		/// </summary>
		public State CurrentState
		{
			get
			{
				return currentState;
			}
			set
			{
				setCurrentState(value);
			}
		}
		#endregion

		#region Public Methods

		/// <summary>
		/// Add a transition
		/// </summary>
		/// <param name="src">The state you would be in when an event occurs</param>
		/// <param name="dest">The state to move to because the event occured</param>
		/// <param name="eventName">the name of the event</param>
		/// <param name="handler">the delegate to execute.  Can be null for no action</param>
		public void AddTransition(State src, State dest, string eventName, EventHandler handler)
		{
			// build the transition string
			string key = getTransitionKey(src, eventName);
			// oops, cant have the exact same transition twice!
			if (transitionTable.ContainsKey(key))
			{
				throw new ApplicationException("State already in transition table");
			}

			TransitionElement e = new TransitionElement(dest, handler);
			transitionTable[key] = e;

		}

		/// <summary>
		/// Handle an event -- transition states if necessary.
		/// </summary>
		/// <param name="eventName">Name of event that occurred</param>
		public void ProcessEvent(string eventName)
		{
			// build the transition key, so we can find out if there is a transition
			string key = getTransitionKey(CurrentState, eventName);

			// if there is no transition for this event, we ignore it.
			if (transitionTable.ContainsKey(key) == false)
			{
				return;
			}

			// The steps now are to leave the current state, execute transition function, enter new state.
			exitCurrentState();
			
			TransitionElement elem = transitionTable[key];
			executeTransition(elem);

			enterCurrentState(elem.NewState);
		}

		#endregion

		#region Private Members

		/// <summary>
		/// Executes a transition function if there is one, also executed any global actions.
		/// </summary>
		/// <param name="elem"></param>
		private void executeTransition(TransitionElement elem)
		{
			// if no transition function, do nothing
			if (elem.TransitionFunction == null)
			{
				return;
			}

			// if there is a global pre-action, execute it.
			if (PreTransition != null)
			{
				PreTransition(elem.NewState);
			}

			// transition
			elem.TransitionFunction(elem.NewState, null);

			// execute post action if any
			if (PostTransition != null)
			{
				PostTransition(elem.NewState);
			}
		}

		private string getTransitionKey(State src, string eventName)
		{
			return string.Format("SM_{0}.{1}", src.Name, eventName);
		}

		private void setCurrentState(State s)
		{
			if (currentState != null)
			{
				exitCurrentState();
			}

			enterCurrentState(s);
		}

		/// <summary>
		/// Enters a new state, and makes it the current state
		/// </summary>
		/// <param name="s"></param>
		private void enterCurrentState(State s)
		{
			// if there is a global action, do it
			if (PreEnter != null)
			{
				PreEnter(s);
			}

			// enter the state and officially make it the current state
			s.Enter();
			currentState = s;

			// if there is a global aftter action, do it
			if (PostEnter != null)
			{
				PostEnter(s);
			}
		}

		/// <summary>
		/// cakked to exit the current state
		/// </summary>
		private void exitCurrentState()
		{
			State s = currentState;

			// execute any global pre-exit actions
			if (PreExit != null)
			{
				PreExit(s);
			}

			// exit the state, and set current to null
			s.Exit();
			currentState = null;

			// execute any post-exit global actions
			if (PostExit != null)
			{
				PostExit(s);
			}
		}

		#endregion

	}
}
