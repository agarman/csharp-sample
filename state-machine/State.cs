using System;
using System.Collections.Generic;
using System.Text;

namespace EchoStarInserter
{
	/// <summary>
	/// This class represents a state that has entry and exit functions associated with it.  You can enter and leave
	/// states.
	/// </summary>
	public class State
	{
		#region Private Members
		/// <summary>
		/// The name of the state
		/// </summary>
		private string name;

		/// <summary>
		/// The delegates that wish to hear about entering the state
		/// </summary>
		public event EventHandler EnterEvent;
		/// <summary>
		/// The delegates that wish to hear about leaving the state
		/// </summary>
		public event EventHandler ExitEvent;
		#endregion

		#region Properties

		/// <summary>
		/// Property that represents the name of the state.  Read-only
		/// </summary>
		public string Name
		{
			get
			{
				return name;
			}
		}
		#endregion

		#region CTORs

		/// <summary>
		/// Ctor that takes the name of the state.
		/// </summary>
		/// <param name="name"></param>
		public State(string name)
		{
			this.name = name;
		}

		#endregion

		#region Public Members

		/// <summary>
		/// This should be called when one wants to enter this state.  It will notify all interested parties
		/// this has occured.
		/// </summary>
		public void Enter()
		{
			// Handle Multi-thread badness by copying ref.
			EventHandler tmp = EnterEvent;

			if (tmp != null)
			{
				tmp(this, null);
			}
		}

		/// <summary>
		/// This method should be called when you wish to leave this state.  It will notify all interested parties.
		/// </summary>
		public void Exit()
		{
			// Handle Multi-thread badness by copying ref.
			EventHandler tmp = ExitEvent;

			if (ExitEvent != null)
			{
				tmp(this, null);
			}
		}

		#endregion

	}
}
