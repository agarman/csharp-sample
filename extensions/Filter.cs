﻿namespace TWC.Services.DataContracts
{
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Linq;
	using System.Runtime.Serialization;

	[DataContract(Namespace = @"http://TWC/Services/ContentData/Data")]
	public class Filter
	{
		#region Fields

		#endregion

		//Need this parameterless constructor in order
		//to be able to serialize!
		public Filter() {}

		public Filter(string fieldName, string fieldValue)
		{
			Name = fieldName;
			Value = fieldValue;
		}

		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// The value of the field.  If the field isn't of type string, it's the
		/// value of the field after ToString().
		/// </summary>
		[DataMember]
		public string Value { get; set; }
	}

	public class FilterCollection : Collection<Filter>
	{
		public object[] this[string key]
		{
			get { return this.Where(t => t.Name == key).ToArray(); }
		}

		public void Add(string key, object val)
		{
			Add(new Filter(key, val.ToString()));
		}

		public bool Contains(string key)
		{
			return this.Find(tu => tu.Name == key) != null;
		}

		/// <summary>
		/// Throws if more than one criteria on the same key.s
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, object> ToDictionary()
		{
			var d = new Dictionary<string, object>();
			foreach (var filter in this)
			{
				d.Add(filter.Name, filter.Value);
			}
			return d;
		}

		public DynamicFilter ToDynamicFilter()
		{
			var f = new DynamicFilter();

			foreach (var item in this)
			{
				f.Add(item.Name, item.Value);
			}

			return f;
		}
	}
}