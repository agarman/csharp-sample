﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace TWC.Services.ExtensionClasses.ObjectExtensions
{
  public static class EnumExtensions
  {
    /// <summary>
    ///   Returns the first character of the DescriptionAttribute
    ///     or default(char) if DescriptionAttribute not defined
    /// </summary>
    /// <param name="value">the enum</param>
    /// <returns>First character of the DescriptionAttribute</returns>
    public static char ToCharCode(this Enum value)
    {
      string description = value.ToDescription();
      return description.Length > 0 ? description[0] : default(char);
    }

    /// <summary>
    ///   Returns the value of the DescriptionAttribute or the value.ToString if
    ///     the DescriptionAttribute is not defined.  
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string ToDescription(this Enum value)
    {
      Type type = value.GetType();
      MemberInfo[] memberInfo = type.GetMember(value.ToString());
      if (memberInfo != null && memberInfo.Length > 0)
      {
        object[] attributes = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
        if (attributes != null && attributes.Length > 0)
          return ((DescriptionAttribute) attributes[0]).Description;
      }
      return value.ToString();
    }
  }
}
