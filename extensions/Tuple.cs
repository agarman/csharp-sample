namespace TWC.Services
{
	/// <summary>
	/// 	This is a mixed typed tuple.
	/// </summary>
	/// <typeparam name = "T"></typeparam>
	/// <typeparam name = "TRest"></typeparam>
	public class Tuple<T, TRest>
	{
		public Tuple(T first, TRest second)
		{
			Head = first;
			Second = second;
		}

		public T Head { get; set; }

		public T First
		{
			get { return Head; }
		}

		public TRest Second { get; set; }
	}

	public abstract class Tuple
	{
		public static Tuple<T, TRest> New<T, TRest>(T first, TRest second)
		{
			return new Tuple<T, TRest>(first, second);
		}

		public static Tuple<T> New<T>(T head, Tuple<T> rest)
		{
			return new Tuple<T>(head, rest);
		}
	}

	/// <summary>
	/// 	This is a homo-typed tuple.
	/// </summary>
	/// <typeparam name = "T"></typeparam>
	public class Tuple<T>
	{
		public Tuple(T head, Tuple<T> rest)
		{
			Head = head;
			Rest = rest;
		}

		public T Head { get; set; }

		public T First
		{
			get { return Head; }
		}

		public Tuple<T> Rest { get; set; }

		public T Second
		{
			get { return Rest.Head; }
		}

		public Tuple<T> Cons(T head, Tuple<T> rest)
		{
			return new Tuple<T>(head, rest);
		}
	}
}