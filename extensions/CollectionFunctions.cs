using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace TWC.Services.ExtensionClasses
{
	public static class CollectionFunctions
	{
		public static void AddRange<T>(this ICollection<T> col, IEnumerable<T> source)
		{
			foreach (var x in source)
			{
				col.Add(x);
			}
		}

		public static void Find<T>(this IEnumerable<T> col, Predicate<T> p, ref T result)
		{
			foreach (var item in col)
			{
				if (!p(item))
				{
					continue;
				}

				result = item;
				return;
			}
		}

		public static T Find<T>(this IEnumerable<T> col, Predicate<T> p)
		{
			var result = default(T);
			Find(col, p, ref result);
			return result;
		}

		public static bool TryGet<T>(this Collection<T> col, T item, out T outItem)
		{
			outItem = default(T);
			var index = col.IndexOf(item);

			if (index == -1)
			{
				return false;
			}
			outItem = col[index];
			return true;
		}

		public static void ForEach<T>(this IEnumerable<T> col, Action<T> action)
		{
			foreach (var item in col)
			{
				action(item);
			}
		}

		public static Collection<K> Reduce<T, K>(this T col, ref Collection<K> result, Predicate<K> p)
			where T : IEnumerable<K>
		{
			result.Clear();

			foreach (var item in col)
			{
				if (p(item))
				{
					result.Add(item);
				}
			}

			return result;
		}
	}
}