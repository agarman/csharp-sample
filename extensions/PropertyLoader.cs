using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace TWC.Services.ExtensionClasses.ObjectExtensions
{
	public static partial class ObjectExtensions
	{
		/// <summary>
		/// 	Sets values of objectToLoad with values provided by dictionary.
		/// </summary>
		/// <remarks>
		/// 	Values are set from the dictionary.  Properties not in the dictionary 
		/// 	will not be attempted to be set.
		/// </remarks>
		public static TDestination LoadFrom<TDestination>(this TDestination destination, Dictionary<string, object> source)
		{
			var dPropNames = Projector<TDestination>.PropertyNames.ToDictionary(p => p);
			foreach (var pair in source)
			{
				if (dPropNames.ContainsKey(pair.Key))
				{
					Projector<TDestination>.Write(destination, pair.Key, pair.Value);
				}
			}

			return destination;
		}

		/// <summary>
		/// 	Copies column values from IDataRecord into destination object's properties.
		/// </summary>
		public static TDestination LoadFrom<TDestination>(this TDestination destination, IDataRecord data)
		{
			var valueCache = new Dictionary<string, object>();
			for (var i = 0; i < data.FieldCount; i++)
			{
				valueCache.Add(data.GetName(i), data.GetValue(i));
			}

            var dPropNames = Projector<TDestination>.MemberNames;
            foreach (var pair in valueCache.Where(p => p.Value != System.DBNull.Value))
            {
                if (dPropNames.ContainsKey(pair.Key))
                {
                    Projector<TDestination>.Write(destination, dPropNames[pair.Key], pair.Value);
                }
            }

            return destination;
		}
	}
}