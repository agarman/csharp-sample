using System;

namespace TWC.Services.ExtensionClasses.ObjectExtensions
{
	public static partial class ObjectExtensions
	{
		public static bool IsIntegral<T>(this T value)
		{
			return IntegralChecker<T>.Check(value);
		}

		public static bool IsNumber<T>(this T value)
		{
			return NumberChecker<T>.Check(value);
		}

		#region Nested type: IntegralChecker

		private class IntegralChecker<T>
		{
			static IntegralChecker()
			{
				var type = typeof (T);
				Result = Check(type);
			}

			public static bool Result { get; private set; }

			private static bool Check(Type type)
			{
				return ((type.IsPrimitive) &&
				        ((type == typeof (int)) ||
				         (type == typeof (long)) ||
				         (type == typeof (byte)) ||
				         (type == typeof (short)) ||
				         (type == typeof (double)) ||
				         (type == typeof (ulong)) ||
				         (type == typeof (ushort)) ||
				         (type == typeof (uint)) ||
				         (type == typeof (sbyte))));
			}

			public static bool Check<TObj>(TObj obj)
			{
				if (typeof (TObj) != typeof (object))
				{
					return Result;
				}
				if (Equals(null, obj))
				{
					return false;
				}
				var type = obj.GetType();
				return Check(type);
			}
		}

		#endregion

		#region Nested type: NumberChecker

		private class NumberChecker<T>
		{
			static NumberChecker()
			{
				var type = typeof (T);
				Result = Check(type);
			}

			public static bool Result { get; private set; }

			private static bool Check(Type type)
			{
				return ((type.IsPrimitive) &&
				        (IntegralChecker<T>.Result ||
				         (type == typeof (float)) ||
				         (type == typeof (decimal))));
			}

			public static bool Check<TObj>(TObj obj)
			{
				if (typeof (TObj) != typeof (object))
				{
					return Result;
				}
				if (Equals(null, obj))
				{
					return false;
				}
				var type = obj.GetType();
				return Check(type);
			}
		}

		#endregion
	}
}