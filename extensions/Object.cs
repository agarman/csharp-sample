﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;

namespace TWC.Services.ExtensionClasses.ObjectExtensions
{
	public static partial class ObjectExtensions
    {
        /// <summary>
        /// Traverses the object graph from root using reflection. Applies the specified
        /// action to each child node in the graph that can be cast to the generic argument
        /// TTarget.
        /// </summary>
        /// <typeparam name="TTarget"></typeparam>
        /// <param name="root"></param>
        /// <param name="onTargetVisited"></param>
        public static void VisitObjectGraph<TTarget>(this object root, Action<TTarget> onTargetVisited)
            where TTarget : class
        {
            VisitObjectGraph<TTarget>(root, true, onTargetVisited);
        }

        /// <summary>
        /// Traverses the object graph from root using reflection. Applies the specified
        /// action to each child node in the graph that can be cast to the generic argument
        /// TTarget.
        /// </summary>
        /// <typeparam name="TTarget"></typeparam>
        /// <param name="root"></param>
        /// <param name="allowRecursion"></param>
        /// <param name="onTargetVisited"></param>
        public static void VisitObjectGraph<TTarget>(this object root, bool allowRecursion, Action<TTarget> onTargetVisited)
            where TTarget : class
        {
            // skip nulls
            if (root == null)
                return;

            Type RootType = root.GetType();

            // skip value types (and strings)
            if (RootType.IsValueType || RootType == typeof(String))
                return;

            // if this is a list, then recursively process all items
            bool IsEnumerable = (RootType.GetInterface("System.Collections.IEnumerable") != null);
            if (IsEnumerable)
            {
                if (allowRecursion)
                {
                    foreach (var item in (root as IEnumerable))
                        VisitObjectGraph<TTarget>(item, allowRecursion, onTargetVisited);
                }
            }
            else
            {
                // also check all public members (non-indexed reference types only)
                var PublicProperties =
                    from prop in RootType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    where prop.PropertyType.IsValueType == false
                    where prop.PropertyType != typeof(string)
                    where prop.GetIndexParameters().Length == 0
                    select prop;
                foreach (var Prop in PublicProperties)
                {
                    // recursively process this property value
                    var PropValue = Prop.GetValue(root, null);
                    VisitObjectGraph<TTarget>(PropValue, allowRecursion, onTargetVisited);
                }
            }

            // check to see if this is one of the objects we are scanning for...
            TTarget CastAsTarget = root as TTarget;
            if (CastAsTarget != null)
            {
                // this is a TTarget, so visit the node
                onTargetVisited(root as TTarget);
            }
        }

        /// <summary>
        /// Examines the root object using reflection. Applies the specified
        /// action to each property that can be cast to the generic argument
        /// TTarget.
        /// </summary>
        /// <typeparam name="TTarget"></typeparam>
        /// <param name="root"></param>
        /// <param name="allowRecursion"></param>
        /// <param name="onTargetVisited"></param>
        public static void VisitObjectProperties<TTarget>(this object root, Action<PropertyInfo, TTarget> onTargetVisited)
            where TTarget : class
        {
            // skip nulls
            if (root == null)
                return;

            Type RootType = root.GetType();

            // check all public members (non-indexed reference types only)
            var PublicProperties =
                from prop in RootType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                where prop.PropertyType.IsValueType == false
                where prop.PropertyType != typeof(string)
                where prop.GetIndexParameters().Length == 0
                select prop;
            foreach (var Prop in PublicProperties)
            {
                // recursively process this property value
                var PropValue = Prop.GetValue(root, null) as TTarget;
                if (PropValue != null)
                    onTargetVisited(Prop, PropValue);
            }
        }
    }
}
