﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TWC.Services.ExtensionClasses.ObjectExtensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<IEnumerable<T>> SplitIntoGroups<T>(this IEnumerable<T> source, int groupSize)
        {
            // copy to temp array to ensure nothing is deferred (deferred execution
            // would lead to a factorial increase in cost due to skip/take)
            T[] All = source.ToArray();

            int Start = 0;
            var Group = All.Take(groupSize);
            while (Group.Any())
            {
                yield return Group;
                Start += groupSize;
                Group = All.Skip(Start).Take(groupSize);
            }
        }

        public static bool ItemsAreSame<T>(this IEnumerable<T> left, IEnumerable<T> right) where T : IComparable
        {
            if (left == null || right == null) return false;

            var count = left.Count();
            if (count != right.Count()) return false;

            var leftOrdered = left.OrderBy(item => item).ToList();
            var rightOrdered = right.OrderBy(item => item).ToList();

            for (int i = 0; i < count; i++)
            {
                if (!Equals(leftOrdered[i], rightOrdered[i])) return false;
            }

            return true;
        }
    }
}
