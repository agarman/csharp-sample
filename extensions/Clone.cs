using System;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Linq.Expressions;

namespace TWC.Services.ExtensionClasses.ObjectExtensions
{
	public static partial class ObjectExtensions
	{
		public static T Clone<T>(this T obj) where T : new()
		{
			return ObjectExtCache<T>.Clone(obj);
		}

		#region Nested type: ObjectExtCache

		private static class ObjectExtCache<T> where T : new()
		{
			private static readonly Func<T, T> Cloner;

			static ObjectExtCache()
			{
				var param = Expression.Parameter(typeof (T), "in");
				var bindings =
					from prop in typeof (T).GetProperties()
					where prop.CanRead && prop.CanWrite
					let column = Attribute.GetCustomAttribute(prop, typeof (ColumnAttribute)) as ColumnAttribute
					where column == null || !column.IsPrimaryKey
					select (MemberBinding) Expression.Bind(prop, Expression.Property(param, prop));

				Cloner = Expression.Lambda<Func<T, T>>(Expression.MemberInit(Expression.New(typeof (T)), bindings), param).Compile();
			}

			public static T Clone(T obj)
			{
				return Cloner(obj);
			}
		}

		#endregion
	}
}