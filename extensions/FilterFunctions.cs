using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TWC.Services.Expressions;

namespace TWC.Services.ExtensionClasses
{
	public static class FilterFunctions
	{
		public static IQueryable FilterWith(this IQueryable source, Expression criteria)
		{
			if (source == null)
			{
				throw new ArgumentException("Source cannot be null");
			}
			if (criteria == null)
			{
				return source.Provider.CreateQuery(source.Expression);
			}

			var query = source.Provider.CreateQuery(Expression.Call(typeof (Queryable),
			                                                        "Where",
			                                                        new[] {source.ElementType},
			                                                        source.Expression,
			                                                        Expression.Quote(criteria)));

			return query;
		}

		public static IQueryable<T> FilterWith<T>(this IEnumerable<T> source, LambdaExpression criteria)
		{
			return FilterWith(source.AsQueryable() as IQueryable, criteria) as IQueryable<T>;
		}

		public static IQueryable FilterWith(this IQueryable source, SExpression criteria)
		{
			if (source == null)
			{
				throw new ArgumentException("Source cannot be null");
			}
			if (criteria == null)
			{
				return source.Provider.CreateQuery(source.Expression);
			}

			var sourcedOperator = new SourcedSExpression(criteria, source);
			var exp = sourcedOperator.ToExpression();

			if (exp == null)
			{
				return source;
			}

			var query = source.Provider.CreateQuery(Expression.Call(typeof (Queryable),
			                                                        "Where",
			                                                        new[] {source.ElementType},
			                                                        source.Expression,
			                                                        Expression.Quote(exp)));

			return query;
		}

		public static IQueryable<T> FilterWith<T>(this IEnumerable<T> source, SExpression criteria)
		{
			return FilterWith(source.AsQueryable() as IQueryable, criteria) as IQueryable<T>;
		}

		public static IQueryable<T> FilterWith<T>(this IQueryable<T> source, SExpression criteria)
		{
			return FilterWith(source as IQueryable, criteria) as IQueryable<T>;
		}

		public static IQueryable<T> FilterWith<T>(this IQueryable<T> source, SearchCriteria filter)
		{
			if (filter == null || filter.Count == 0)
			{
				return filter != null ? source.Take(filter.MaxResults()) : source;
			}

			var expr = filter.ToExpresssion();

			return (FilterWith(source as IQueryable, expr) as IQueryable<T>).Take(filter.MaxResults());
		}
	}
}