using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TWC.Services.ExtensionClasses.ObjectExtensions
{
	public class PropertyFilter<T>
	{
		private readonly List<Tuple<string, Regex>> _regexes = new List<Tuple<string, Regex>>();

		/// <summary>
		/// 	ctor for single filter value
		/// </summary>
		/// <param name = "f">Single string filter matched against all properties</param>
		public PropertyFilter(string f)
		{
			var regex = new Regex(f, RegexOptions.IgnoreCase | RegexOptions.Compiled);
			foreach (var p in ObjectExtensions.Projector<T>.PropertyNames)
			{
				_regexes.Add(Tuple.Create(p, regex));
			}
		}

        /// <summary>
        /// Returns true if t instance has any properties which value match regex
        /// </summary>
		public bool MatchAny(T t)
		{
			foreach (var tuple in _regexes)
			{
				var s = GetValueAsString(t, tuple.Item1);
				if (tuple.Item2.IsMatch(s))
				{
					return true;
				}
			}

			return false;
		}

        /// <summary>
        /// Creates a lazy list with all values of properties which values match regex pattern
        /// </summary>
        public IEnumerable<string> GetMatches(T t)
        {
            foreach (var tuple in _regexes)
            {
                var s = GetValueAsString(t, tuple.Item1);
                if (tuple.Item2.IsMatch(s))
                {
                    yield return s;
                }
            }
        }

        #region private

        /// <summary>
		/// 	Reads and returns the value of a property of a T instance as a string
		/// </summary>
		private static string GetValueAsString(T t, string property)
		{
			var value = ObjectExtensions.Projector<T>.Read(t, property);
			return value == null
			       	? string.Empty
			       	: value.ToString();
        }

        #endregion
    }
}