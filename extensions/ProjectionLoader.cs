using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace TWC.Services.ExtensionClasses.ObjectExtensions
{
	public static partial class ObjectExtensions
	{
		/// <summary>
		/// 	Makes a best attempt to copy properties by name from the source object
		/// 	to the destination object.
		/// </summary>
		public static TDestination ProjectInto<TSource, TDestination>(this TSource source, TDestination destination)
		{
			var sPropNames = Projector<TSource>.PropertyNames;
			var dPropNames = Projector<TDestination>.PropertyNames;
			var intersect = dPropNames.Intersect(sPropNames);

			foreach (var s in intersect)
			{
				var value = Projector<TSource>.Read(source, s);
				Projector<TDestination>.Write(destination, s, value);
			}

			return destination;
		}

		public static void AddRangeWithProjection<TSource, TDestination>(this ICollection<TDestination> destination,
		                                                                 IEnumerable<TSource> source)
			where TDestination : new()
		{
			foreach (var item in source.Select(s => s.ProjectInto(new TDestination())))
			{
				destination.Add(item);
			}
		}

		#region Nested type: Projector

		public static class Projector<T> {
                    private static readonly Dictionary<string, MethodInfo> ParseMethods = new Dictionary<string, MethodInfo>();
                    private static readonly List<string> PNames = new List<string>();
                    private static readonly Dictionary<string, string> MNames = new Dictionary<string, string>();
                    public static readonly Dictionary<string, PropertyInfo> PropertyInfos = new Dictionary<string, PropertyInfo>();

                    static Projector()
                    {
                        var props = from p in typeof (T).GetProperties()
                                    where p.CanRead
                                    select new { p.Name, p };

                        foreach (var prop in props) {
                            PropertyInfos.Add(prop.Name, prop.p);
                            PNames.Add(prop.Name);
                            MNames.Add(GetDataMemberName(prop.p), prop.Name);
                            var pm = prop.p.PropertyType.GetMethod("Parse", BindingFlags.Public | BindingFlags.Static, null,
                                                                   new[] {typeof (String)}, null);
                            ParseMethods.Add(prop.Name, pm);
                        }
                    }
                    /// <summary>
                    /// 	Readable properties on a type.
                    /// </summary>
                    public static IEnumerable<string> PropertyNames {
                        get { return PNames; }
                    }

                    /// <summary>
                    /// Dictionary of <DataMemberAttribute.Name, Property.Name>
                    /// of readable properties on a type. If DataMemberAttribute is not found, then Property.Name is used instead
                    /// </summary>
                    public static IDictionary<string, string> MemberNames {
                        get { return MNames; }
                    }

                    public static object Read(T source, string name) {
                        if (PropertyInfos[name].GetIndexParameters().Count() > 0) {
                            // Note: returning if property is indexed, may be fixed differently!?
                            System.Diagnostics.Debug.WriteLine("Property {0} is indexed, aborting projection", name);
                            return null;
                        }

                        if (source == null) return null;

                        return PNames.Contains(name) ? PropertyInfos[name].GetValue(source, null) : null;
                    }

                    public static void Write<TValue>(T destination, string name, TValue value) {
                        if ((!typeof (TValue).IsValueType) && Equals(null, value)) {
                            return;
                        }

				var destinationProperty = PropertyInfos[name];
				//TODO: refactor this if statement out into a static constructor of projector <>
				if (destinationProperty.CanWrite == false)
				{
					return;
				}

				var propertyType = destinationProperty.PropertyType;
				var parseMethod = ParseMethods[name];

				if (propertyType.IsAssignableFrom(value.GetType())) // In tests:  this is 74% of hits
				{
					destinationProperty.SetValue(destination, value, null);
				}
				else if (propertyType.Equals(typeof (string))) // In tests: this is 26% of hits
				{
					destinationProperty.SetValue(destination, value.ToString(), null);
				}
				else if (propertyType.IsEnum) // Minor portion
				{
					try
					{
						if (value.IsIntegral())
						{
							destinationProperty.SetValue(destination, Enum.ToObject(propertyType, Convert.ToInt64(value)), null);
						}
						else
						{
							// TODO: add case invariant search if no direct parse
							destinationProperty.SetValue(destination, Enum.Parse(propertyType, value.ToString()), null);
						}
					}
					catch (Exception e)
					{
						Console.WriteLine(e);
						Console.WriteLine(string.Format(@"Could not match {0} with enum: {1}", value, propertyType.Name));
					}
				}
				else if (parseMethod != null) // Occurs infrequently.
				{
					try
					{
						destinationProperty.SetValue(destination, parseMethod.Invoke(destination, new Object[] {value.ToString()}), null);
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex);
						Console.WriteLine(string.Format("Parse didn't work for type {0} with value {1}", propertyType, value));
					}
				}
				else
				{
					Console.WriteLine(@"Couldn't set: {0} with type {1}.", destinationProperty.Name, value.GetType());
				}
			}

            private static string GetDataMemberName(PropertyInfo info)
            {
                var attribute = Attribute.GetCustomAttributes(info).Where(a => a.GetType() == typeof(DataMemberAttribute)).FirstOrDefault();
                if (attribute == null)
                {
                    return info.Name;
                }
                else
                {
                    var name = ((DataMemberAttribute)attribute).Name;
                    return string.IsNullOrEmpty(name) ? info.Name : name;
                }
            }
		}

		#endregion
	}
}
